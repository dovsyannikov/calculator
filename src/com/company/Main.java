package com.company;

import java.util.Scanner;

import static com.company.CalculationSum.*;

public class Main {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Это Калькулятор, введите первое число");
        int num1 = getInt();
        System.out.println("введите второе число");
        int num2 = getInt();
        System.out.println("введите тип операции: (/,*,-,+)");
        String operation = getOperation();
        int result = calc(num1, num2, operation);
        System.out.println("Результат операции: " + result);
    }
}
