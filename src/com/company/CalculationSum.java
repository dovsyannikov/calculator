package com.company;

import java.util.Scanner;

public class CalculationSum {

    static Scanner scanner = new Scanner(System.in);

    public static int calc(int num1, int num2, String operation) {
        int result;
        switch (operation){
            case ("/"):
                result = num1 / num2;
                break;
            case ("+"):
                result = num1 + num2;
            case ("-"):
                result = num1 - num2;
                break;
            case ("*"):
                result = num1 * num2;
                break;
            default:
                System.out.println("Smth went wrong");
                result = 0;
                break;
        }
        return result;
    }

    public static String getOperation() {
        return scanner.next();
    }

    public static int getInt() {
        return scanner.nextInt();
    }

}
